function log(title, dict) {
  let lines = [];
  let maxKeyWidth = 0;
  let maxValueWidth = 0;
  Object.keys(dict).forEach(function (key) {
    if (key.length > maxKeyWidth) {
      maxKeyWidth = key.length;
    }

    let value = "" + dict[key];
    if (value.length > maxValueWidth) {
      maxValueWidth = value.length;
    }
  });
  Object.keys(dict).forEach(function (key, i, t) {
    let value = "" + dict[key];
    let leftChar = "\u2503";
    let rightChar = "\u2503";

    lines.push(leftChar + key + "  " + " ".repeat(maxKeyWidth - key.length + 1) +
      " ".repeat(maxValueWidth - value.length + 1) + value + " " + rightChar);
  });
  let firstLine = "\u250f" + "\u2501".repeat(maxKeyWidth + maxValueWidth + 5) + "\u2513";
  firstLine = firstLine.split("");
  firstLine.splice(Math.floor(firstLine.length / 2) - Math.ceil(title.length / 2), title.length +
    2, " ", ...title.split(""), " ");

  lines.unshift(firstLine.join(""));
  lines.push("\u2517" + "\u2501".repeat(maxKeyWidth + maxValueWidth + 5) + "\u251b");
  lines.push("");
  lines.forEach(function (line) {
    console.log(line);
  });
}
