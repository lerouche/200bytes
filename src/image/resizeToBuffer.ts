import { ImageSource } from "./ImageSource";
import gm from "gm";

export default function resizeToBuffer(src: ImageSource, width: number, height?: number): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    gm(src)
      .strip()
      .resize(width, height)
      .toBuffer((err, img) => {
        if (err) {
          reject(err);
        } else {
          resolve(img);
        }
      });
  });
}
