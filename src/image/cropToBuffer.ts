import { ImageSource } from "./ImageSource";
import gm from "gm";

export default function cropToBuffer(src: ImageSource, width: number, height: number, x: number, y: number): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    gm(src)
      .strip()
      .crop(width, height, x, y)
      .toBuffer((err, img) => {
        if (err) {
          reject(err);
        } else {
          resolve(img);
        }
      });
  });
}
