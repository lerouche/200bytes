import { ImageSource } from "./ImageSource";
import { ColorObject, normaliseToColorObject } from "../color/Color";

export default function getDominantColor(src: ImageSource): Promise<ColorObject> {
  return new Promise((resolve, reject) => {
    Vibrant.from(src)
      .getPalette((err, palette) => {
        if (err) {
          reject(err);
          return;
        }

        let color = palette[Object.keys(palette).reduce((previousType, currentType) => {
          if (!palette[previousType] || (palette[currentType] && palette[currentType].getPopulation() > palette[previousType].getPopulation())) {
            return currentType;
          } else {
            return previousType;
          }
        })];

        resolve(normaliseToColorObject(color.getRgb()));
      });
  });
}
