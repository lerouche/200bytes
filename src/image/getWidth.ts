import { ImageSource } from "./ImageSource";
import gm from "gm";

export default function getWidth(src: ImageSource): Promise<number> {
  return new Promise((resolve, reject) => {
    gm(src)
      .size((err, size) => {
        if (err) {
          reject(err);
        } else {
          resolve(size.width);
        }
      });
  });
}
