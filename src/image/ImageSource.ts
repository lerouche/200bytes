export type ImageSource = string | Buffer | NodeJS.ReadableStream;
