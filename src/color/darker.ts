import { Color, normaliseToColorObject, ColorObject } from "./Color";

// [FUNCTION] Darken RGB by fraction (from 0 to 1).
// Provide an array or three arguments: r, g, b
export default function darker(color: Color, fraction: number): ColorObject {
  let obj = normaliseToColorObject(color);
  return {
    r: Math.max(0, Math.round(obj.r - 255 * fraction)),
    g: Math.max(0, Math.round(obj.g - 255 * fraction)),
    b: Math.max(0, Math.round(obj.b - 255 * fraction)),
  };
}
