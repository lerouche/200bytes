export interface ColorObject {
  r: number;
  g: number;
  b: number;
}

export type Color = [number, number, number] | ColorObject;

export function normaliseToColorObject(color: Color): ColorObject {
  if (Array.isArray(color)) {
    return {
      r: color[0],
      g: color[1],
      b: color[2],
    };

  } else {
    return color;
  }
}
