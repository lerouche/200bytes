import { Color, normaliseToColorObject } from "./Color";

export default function perceivedBrightness(color: Color): number {
  let obj = normaliseToColorObject(color);
  return (0.299 * obj.r + 0.587 * obj.g + 0.114 * obj.b) / 255;
}
