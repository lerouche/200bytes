import { Color } from "./Color";
import perceivedBrightness from "./perceivedBrightness";

export default function isLight(color: Color): boolean {
  return perceivedBrightness(color) > 0.5;
}
