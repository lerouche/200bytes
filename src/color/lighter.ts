import { Color, normaliseToColorObject, ColorObject } from "./Color";

export default function lighter(color: Color, fraction: number): ColorObject {
  let obj = normaliseToColorObject(color);
  return {
    r: Math.min(255, Math.round(obj.r + 255 * fraction)),
    g: Math.min(255, Math.round(obj.g + 255 * fraction)),
    b: Math.min(255, Math.round(obj.b + 255 * fraction)),
  };
}
