import minimist from "minimist";
import fs from "fs";
import Path from "path";
import getWidth from "./image/getWidth";
import resizeToBuffer from "./image/resizeToBuffer";
import cropToBuffer from "./image/cropToBuffer";
import getDominantColor from "./image/getDominantColor";
import { ColorObject } from "./color/Color";
import darker from "./color/darker";
import lighter from "./color/lighter";
import perceivedBrightness from "./color/perceivedBrightness";

const ARGS = minimist(process.argv.slice(2));

const SOURCE_DIR = ARGS.src;
const DESTINATION_DIR = ARGS.dest;

fs.readdir(SOURCE_DIR, (err, files) => {
  if (err) {
    throw err;
  }

  files.forEach(file => {
    if (!/^[0-9]+\.jpg$/.test(file)) {
      return;
    }

    let path = Path.join(SOURCE_DIR, file);

    let packed = [];

    let baseName = file.slice(0, file.indexOf("."));
    let width: number;

    let headerImg: Buffer;
    let headerImg40: Buffer;
    let logoImg: Buffer;
    let userButtonImg: Buffer;

    let headerRgb: ColorObject;
    let logoRgb: ColorObject;
    let userButtonRgb: ColorObject;

    let logoColor;
    let notificationsBackground, notificationsBorder, notificationsColor;
    let userButtonBackground;

    getWidth(path)
      .then(res => {
        width = res;
        return cropToBuffer(path, width, 100, 0, 0);
      })
      .then(b => {
        headerImg = b;
        return resizeToBuffer(b, 40);
      })
      .then(b => {
        headerImg40 = b;
        return cropToBuffer(headerImg, 150, 100, 0, 0);
      })
      .then(b => {
        logoImg = b;
        return cropToBuffer(headerImg, 250, 100, width - 250, 0);
      })
      .then(b => {
        userButtonImg = b;
        return getDominantColor(headerImg);
      })
      .then(rgb => {
        headerRgb = rgb;
        return getDominantColor(logoImg);
      })
      .then(rgb => {
        logoRgb = rgb;
        return getDominantColor(userButtonImg);
      })
      .then(rgb => {
        userButtonRgb = rgb;
      })
      .then(() => {
        [logoRgb, headerRgb, userButtonRgb].forEach((rgb, i) => {
          let perc = perceivedBrightness(rgb);

          let slightDark = darker(rgb, 0.15);
          let moderateDark = darker(rgb, 0.35);
          let heavyDark = darker(rgb, 0.5);
          let slightLight = lighter(rgb, 0.15);
          let moderateLight = lighter(rgb, 0.35);
          let heavyLight = lighter(rgb, 0.5);

          if (i == 0) {
            // 81 - 100
            if (perc > 0.8) {
              packed = packed.concat(heavyDark);
              logoColor = "rgb(" + heavyDark.join(",") + ")";
            }

            // 61 - 80
            else if (perc > 0.6) {
              packed = packed.concat(heavyDark);
              logoColor = "rgb(" + heavyDark.join(",") + ")";
            }

            // 51 - 60
            else if (perc > 0.5) {
              packed = packed.concat(heavyDark);
              logoColor = "rgb(" + heavyDark.join(",") + ")";
            }

            // 41 - 50
            else if (perc > 0.4) {
              packed = packed.concat(heavyLight);
              logoColor = "rgb(" + heavyLight.join(",") + ")";
            }

            // 21 - 40
            else if (perc > 0.2) {
              packed = packed.concat(heavyLight);
              logoColor = "rgb(" + heavyLight.join(",") + ")";
            }

            // 0 - 20
            else {
              packed = packed.concat(heavyLight);
              logoColor = "rgb(" + heavyLight.join(",") + ")";
            }
          }

          else if (i == 1) {
            packed = packed.concat(rgb);
            notificationsBackground = "rgba(" + rgb.join(",") + ",0.8)";

            if (perc > 0.5) {
              packed = packed.concat(slightDark);
              notificationsBorder = "2px solid rgb(" + slightDark.join(",") + ")";
              packed = packed.concat(moderateDark);
              notificationsColor = "rgb(" + moderateDark.join(",") + ")";
            } else {
              packed = packed.concat(slightLight);
              notificationsBorder = "2px solid rgb(" + slightLight.join(",") + ")";
              packed = packed.concat(moderateLight);
              notificationsColor = "rgb(" + moderateLight.join(",") + ")";
            }
          }

          else if (i == 2) {
            if (perc > 0.5) {
              packed.push(1);
              packed.push(Math.round(((1 - perc) * 0.3 + 0.15) * 100));
              userButtonBackground = "rgba(20,20,20," + JSVF.round((1 - perc) * 0.3 + 0.15, -2) + ")";
            } else {
              packed.push(0);
              packed.push(Math.round((0.5 * perc + 0.15) * 100));
              userButtonBackground = "rgba(255,255,255," + JSVF.round(0.5 * perc + 0.15, -2) + ")";
            }
          }

          else {
            throw new Error("Ooops");
          }
        });

        fs.writeFileSync(__dirname + "/../../dist/wp/" + baseName +
          ".data", Buffer.concat([Buffer.from(packed), headerImg40]));

        fs.writeFileSync(__dirname + "/../../dist/wp/" + baseName + ".jpg", headerImg);
      })
      .catch(err => {
        console.error(err);
        process.exit(1);
      });
  });
});
